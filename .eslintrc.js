module.exports = {
  "extends": [
    "plugin:flowtype/recommended",
    "plugin:prettier/recommended",
    "react-app",
    "prettier",
  ],
  "rules": {
    "flowtype/no-types-missing-file-annotation": "off",
    "max-len": ["error", {
      "code": 120,
      "ignoreUrls": true,
      "ignoreStrings": true,
      "ignoreRegExpLiterals": true,
      "ignoreTemplateLiterals": true
    }],
  }
};
