import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./app/App";
import registerServiceWorker from "./registerServiceWorker";

const root: ?Element = document.getElementById("root");

ReactDOM.render(<App />, root);
registerServiceWorker();
