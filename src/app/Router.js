import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";
import { TransitionGroup, CSSTransition } from "react-transition-group";

import HomePage from "./pages/Home/HomePage";
import ProductsPage from "./pages/Products/ProductsPage";
import Header from "./components/Header/Header";
import ShoppingCenterPage from "./pages/ShoppingCenter/ShoppingCenterPage";
import PricingPage from "./pages/Pricing/PricingPage";

type Props = {};

export default class RouteView extends Component<Props> {
  render() {
    return (
      <Route
        render={({ location }) => (
          <section className="app">
            <Header location={location} />
            <TransitionGroup>
              <CSSTransition
                key={location.pathname}
                classNames="fade"
                timeout={300}
              >
                <Switch location={location}>
                  <Route exact path="/" component={HomePage} />
                  <Route path="/products" component={ProductsPage} />
                  <Route
                    path="/solutions/shopping-centers"
                    component={ShoppingCenterPage}
                  />
                  <Route path="/pricing" component={PricingPage} />
                  <Route path="/company" component={HomePage} />
                  <Route path="/resources" component={HomePage} />
                  <Route render={() => <div>Not Found</div>} />
                </Switch>
              </CSSTransition>
            </TransitionGroup>
          </section>
        )}
      />
    );
  }
}
