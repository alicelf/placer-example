import React, { Component } from "react";
import { BrowserRouter } from "react-router-dom";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import RouteView from "./Router";
import "./ResponsiveUtils.css";

const theme = createMuiTheme({
  typography: {
    fontFamily: '"Open Sans", sans-serif'
  },
  palette: {
    primary: {
      main: "#5e63e4"
    }
  },
  overrides: {
    MuiButton: {
      root: {
        borderRadius: "3px",
        textTransform: "none",
        fontWeight: "600"
      },
      focusVisible: {
        boxShadow: "none"
      },
      contained: {
        boxShadow: "none",
        "&:active": {
          boxShadow: "none"
        }
      }
    },
    MuiPaper: {
      rounded: {
        borderRadius: "3px"
      }
    }
  }
});

export default class App extends Component {
  render() {
    return (
      <MuiThemeProvider theme={theme}>
        <BrowserRouter>
          <RouteView />
        </BrowserRouter>
      </MuiThemeProvider>
    );
  }
}
