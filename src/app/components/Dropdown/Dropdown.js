import React, { Component } from 'react';
import { Button, Paper, MenuList } from '@material-ui/core';

import './Dropdown.css';

export default class Dropdown extends Component {

  static defaultProps = {
    label: 'Menu',
    className: '',
    disableTouchRipple: false,
    style: {},
    classes: {
      button: '',
      root: '',
      paper: ''
    }
  };

  render() {
    return (
        <section className={`pl-dropdown ${this.props.classes.root}`} style={this.props.style}>
          <Button aria-haspopup="true"
                  classes={{root: 'pl-dropdown-button'}}
                  className={this.props.classes.button}
                  disableTouchRipple={this.props.disableTouchRipple}
          >
            {this.props.label}
          </Button>
          <Paper className={`pl-dropdown-paper ${this.props.classes.paper}`}>
            <MenuList onClick={this.handleClose} classes={{root: 'pl-dropdown-menu'}}>
              {this.props.children}
            </MenuList>
          </Paper>
        </section>
    );
  }
}

