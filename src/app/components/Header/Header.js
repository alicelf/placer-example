import React, { Component } from "react";
import { Link } from "react-router-dom";

import "./Header.css";
import logo from "assets/logo/logo.svg";
import Navigation from "../Navigation/Navigation";
import { Button } from "@material-ui/core";
import InnerContent from "../InnerContent/InnerContent";

export default class Header extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isScrolled: false
    };
  }

  onLoginClick = () => {
    console.log("LOGIN");
  };

  onGetStarted = () => {
    console.log("Get Started");
  };

  componentDidMount() {
    window.addEventListener("scroll", this.scrollHandler.bind(this), {
      passive: true
    });
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.scrollHandler.bind(this));
  }

  scrollHandler() {
    this.setState({
      isScrolled: window.scrollY > 0
    });
  }

  render() {
    return (
      <header
        className={`pl-header${this.state.isScrolled ? " is-scrolled" : ""}`}
      >
        <InnerContent className="pl-header-inner-component">
          <Link to="/">
            <img src={logo} className="logo" alt="logo" />
          </Link>
          <Navigation location={this.props.location}/>
          <div className="pl-header-buttons">
            <Button onClick={this.onLoginClick} color="primary">
              LOGIN
            </Button>
            <Button
              onClick={this.onGetStarted}
              variant="contained"
              color="primary"
            >
              Get Started
            </Button>
          </div>
        </InnerContent>
      </header>
    );
  }
}
