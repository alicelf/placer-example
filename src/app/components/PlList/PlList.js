import React, { Component } from "react";
import { checkmark } from "assets";
import "./PlList.css";

type Props = {
  listData: Array<{ type: string, description: string }>
};

class PlList extends Component<Props> {
  static defaultProps = {
    listData: []
  };

  render() {
    const { listData } = this.props;
    if (!listData.length) {
      return null;
    }

    return (
      <ul className="features-list">
        {listData.map(item => (
          <li
            key={item.description}
            className={`features-list-item ${item.type}`}
          >
            <img src={checkmark} alt="checkmark" />
            <span>{item.description}</span>
          </li>
        ))}
      </ul>
    );
  }
}

export default PlList;
