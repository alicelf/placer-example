import React, { Component } from "react";
import "./Page.css";

type Props = {
  className: string,
  children?: React.Node | React.ChildrenArray<React.Node>
};

export default class Page extends Component<Props> {
  static defaultProps = {
    className: ""
  };

  render() {
    return (
      <section className={`pl-page ${this.props.className}`}>
        {this.props.children}
      </section>
    );
  }
}
