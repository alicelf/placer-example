import React, { Component } from "react";
import "./MiddleTitle.css";

class MiddleTitle extends Component {
  render() {
    return <h2 className="pl-sub-title-sm">{this.props.children}</h2>;
  }
}

export default MiddleTitle;
