import * as React from "react";
import { Component } from "react";
import "./SubTitle.css";

type Props = {
  className: string,
  children: React.Node | React.ChildrenArray<React.Node>
};

class SubTitle extends Component<Props> {
  static defaultProps = {
    className: ""
  };

  render() {
    return (
      <h2 className={`pl-sub-title ${this.props.className}`}>
        {this.props.children}
      </h2>
    );
  }
}

export default SubTitle;
