import React, {Component} from 'react';
import './GoogleMap.css';
import GoogleMapLoader from 'app/services/GoogleMapLoader/GoogleMapLoader';
import GoogleMapContext from 'app/services/GoogleMapContext/GoogleMapContext';

export default class GoogleMap extends Component {

  static defaultProps = {
    className: ''
  };

  constructor(props) {
    super(props);

    this.mapEl = React.createRef();
    this.mapOptions = GoogleMap.defaultOptions(this.props.options);
    this.mapPromise = this.createMapPromise();
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.options) {
      this.mapPromise.then((obj) => {
        obj.map.setCenter(nextProps.options.center);
        obj.map.setZoom(nextProps.options.zoom);
      });
    }
  }

  createMapPromise() {
    return GoogleMapLoader.load(process.env.REACT_APP_GOOGLE_KEY).then((maps) => {
      return {
        maps,
        map: new maps.Map(this.mapEl.current, this.mapOptions)
      };
    });
  }

  static defaultOptions(options = {}) {
    return Object.assign({
      zoom: 4,
      center: {
        lat: 37.772041,
        lng:  -122.445074
      }, // USA
      mapTypeControl: false,
      fullscreenControl: false,
      disableDefaultUI: true,
      scrollwheel: false,
      showMarkersLabel: true,
      draggable: false,
      zoomControl: false,
      disableDoubleClickZoom: true,
      draggableCursor: 'default',
      styles: [
        {
          "featureType": "administrative",
          "elementType": "all",
          "stylers": [{
            "visibility": "on"
          }]
        },
        {
          "featureType": "administrative",
          "elementType": "labels",
          "stylers": [{
            "weight": "1.00"
          }]
        },
        {
          "featureType": "administrative",
          "elementType": "labels.text.fill",
          "stylers": [{
            "color": "#858383"
          }]
        },
        {
          "featureType": "administrative.neighborhood",
          "elementType": "labels",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "landscape",
          "elementType": "all",
          "stylers": [{
            "color": "#f3f3f3"
          }, {
            "visibility": "on"
          }]
        },
        {
          "featureType": "landscape",
          "elementType": "labels.text",
          "stylers": [{
            "visibility": "on"
          }]
        },
        {
          "featureType": "landscape",
          "elementType": "labels.text.fill",
          "stylers": [{
            "visibility": "on"
          }, {
            "color": "#ffffff"
          }]
        },
        {
          "featureType": "landscape",
          "elementType": "labels.text.stroke",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "poi.attraction",
          "elementType": "all",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "poi.attraction",
          "elementType": "labels",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "poi.business",
          "elementType": "all",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "poi.business",
          "elementType": "labels",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "poi.government",
          "elementType": "all",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "poi.government",
          "elementType": "labels",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "poi.medical",
          "elementType": "all",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "poi.medical",
          "elementType": "labels",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "poi.park",
          "elementType": "geometry.fill",
          "stylers": [{
            "visibility": "on"
          }, {
            "color": "#d8e9d9"
          }]
        },
        {
          "featureType": "poi.park",
          "elementType": "labels",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "poi.place_of_worship",
          "elementType": "all",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "poi.place_of_worship",
          "elementType": "labels",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "poi.school",
          "elementType": "all",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "poi.school",
          "elementType": "labels",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "poi.sports_complex",
          "elementType": "all",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "poi.sports_complex",
          "elementType": "labels",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "road",
          "elementType": "all",
          "stylers": [{
            "saturation": -100
          }, {
            "lightness": 45
          }]
        },
        {
          "featureType": "road.highway",
          "elementType": "all",
          "stylers": [{
            "visibility": "simplified"
          }]
        },
        {
          "featureType": "road.arterial",
          "elementType": "labels.icon",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "road.local",
          "elementType": "all",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "transit",
          "elementType": "all",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "water",
          "elementType": "all",
          "stylers": [{
            "color": "#aacbff"
          }, {
            "visibility": "on"
          }]
        },
        {
          "featureType": "water",
          "elementType": "labels.text",
          "stylers": [{
            "visibility": "on"
          }, {
            "color": "#ffffff"
          }]
        }
      ]
    }, options);
  };

  render() {
    return (
        <GoogleMapContext.Provider value={this.mapPromise}>
          <section className={`pl-google-map-wrapper ${this.props.className}`}>
            <div className="pl-google-map" ref={this.mapEl}/>
            {this.props.children}
          </section>
        </GoogleMapContext.Provider>
    );
  }
}
