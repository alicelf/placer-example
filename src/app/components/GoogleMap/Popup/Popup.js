import React, {Component} from 'react';
import './Popup.css';

import GoogleMapContext from 'app/services/GoogleMapContext/GoogleMapContext';
import { Paper, ClickAwayListener } from '@material-ui/core';

class PopupContent extends Component {
  static defaultProps = {
    className: '',
    longitude: 0,
    latitude: 0
  };

  constructor(props) {
    super(props);

    this.popupEl = React.createRef();

    this.state = {
      position: {}
    };

    this.timeID = null;

    this.initPosition();
  }

  componentDidMount() {
    window.addEventListener('resize', this.onResize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.onResize);
  }

  onResize = () => {
    console.log(this);
    window.clearTimeout(this.timeID);
    this.timeID = window.setTimeout(() => {
      this.props.mapPromise.then((obj) => {
        this.calcPosition(obj);
      });
    }, 50);
  };

  initPosition() {
    this.props.mapPromise.then((obj) => {
      if (obj.map.getProjection()) {
        this.calcPosition(obj);
      } else {
        this.addListener(obj);
      }
    });
  }

  addListener(obj) {
    obj.maps.event.addListenerOnce(obj.map, 'idle', this.calcPosition.bind(this, obj));
  }

  calcPosition(obj) {
    let scale = Math.pow(2, obj.map.getZoom());
    let projection = obj.map.getProjection();
    let marketLatLng = new obj.maps.LatLng({lat: this.props.latitude, lng: this.props.longitude});
    let topRight = projection.fromLatLngToPoint(obj.map.getBounds().getNorthEast());
    let bottomLeft = projection.fromLatLngToPoint(obj.map.getBounds().getSouthWest());
    let marketPoint = projection.fromLatLngToPoint(marketLatLng);

    this.setState({
      position: {
        left: ((marketPoint.x - bottomLeft.x) * scale) - this.popupEl.current.clientWidth / 2 | 0,
        top: ((marketPoint.y - topRight.y) * scale) - this.popupEl.current.clientHeight | 0
      }
    });
  }

  handleClose = () => {
    this.props.onClose && this.props.onClose();
  };

  render() {
    let {mapPromise, ...other} = this.props;
    return (
      <section {...other} className={`pl-google-popup`}
               style={this.state.position} ref={this.popupEl}>
        <ClickAwayListener onClickAway={this.handleClose}>
          <Paper classes={{root: this.props.className}}>
            {this.props.children}
          </Paper>
        </ClickAwayListener>
      </section>
    );
  }
}

export default class Popup extends Component {
  render() {
    return (
        <GoogleMapContext.Consumer>
          {(mapPromise) => (
              <PopupContent {...this.props} mapPromise={mapPromise}>
                {this.props.children}
              </PopupContent>
          )}
        </GoogleMapContext.Consumer>
    );
  }
}
