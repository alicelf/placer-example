import React, {Component} from 'react';
import './Marker.css';

import GoogleMapContext from 'app/services/GoogleMapContext/GoogleMapContext';

class MarkerContent  extends Component {
  static defaultProps = {
    className: '',
    longitude: 0,
    latitude: 0
  };

  constructor(props) {
    super(props);

    this.state = {
      position: {}
    };

    this.timeID = null;

    this.initPosition();
  }

  componentDidMount() {
    window.addEventListener('resize', this.onResize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.onResize);
  }

  onResize = () => {
    window.clearTimeout(this.timeID);
    this.timeID = window.setTimeout(() => {
      this.props.mapPromise.then((obj) => {
        this.calcPosition(obj);
      });
    }, 50);
  };

  initPosition() {
    this.props.mapPromise.then((obj) => {
      if (obj.map.getProjection()) {
        this.calcPosition(obj);
      } else {
        this.addListener(obj);
      }
    });
  }

  addListener(obj) {
    obj.maps.event.addListenerOnce(obj.map, 'idle', this.calcPosition.bind(this, obj));
  }

  calcPosition(obj) {
    let scale = Math.pow(2, obj.map.getZoom());
    let projection = obj.map.getProjection();
    let marketLatLng = new obj.maps.LatLng({lat: this.props.latitude, lng: this.props.longitude});
    let topRight = projection.fromLatLngToPoint(obj.map.getBounds().getNorthEast());
    let bottomLeft = projection.fromLatLngToPoint(obj.map.getBounds().getSouthWest());
    let marketPoint = projection.fromLatLngToPoint(marketLatLng);

    this.setState({
      position: {
        left: (marketPoint.x - bottomLeft.x) * scale | 0,
        top: (marketPoint.y - topRight.y) * scale | 0
      }
    });
  }

  render() {
    let {mapPromise, ...other} = this.props;
    return (
        <div {...other}
             className={`pl-google-market ${this.props.className}`}
             style={this.state.position}>
          {this.props.children}
        </div>
    );
  }
}

export default class Marker extends Component {
  render() {
    return (
        <GoogleMapContext.Consumer>
          {(mapPromise) => (
              <MarkerContent {...this.props} mapPromise={mapPromise}>
                {this.props.children}
              </MarkerContent>
          )}
        </GoogleMapContext.Consumer>
    );
  }
}
