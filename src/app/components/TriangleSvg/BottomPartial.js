import React, { Component } from "react";

class BottomPartial extends Component {
  static defaultProps = {
    className: ""
  };

  render() {
    return (
      <div
        {...this.props}
        className={`${this.props.className} bottomPartGradient`}
      >
        <svg
          width="100%"
          height="370"
          viewBox="0 0 100 110"
          preserveAspectRatio="none"
          gradientTransform="rotate(13)"
        >
          <defs>
            <linearGradient id="bottomPart" x1="0%" y1="0%" x2="100%" y2="0%">
              <stop
                offset="0%"
                style={{ stopColor: "#6269e6", stopOpacity: 1 }}
              />
              <stop
                offset="100%"
                style={{ stopColor: "#6f7eea", stopOpacity: 1 }}
              />
            </linearGradient>
          </defs>
          <polygon points="0,0 105,0 0,100" fill="url(#bottomPart)" />
        </svg>
      </div>
    );
  }
}

export default BottomPartial;
