import React, { Component } from "react";

type Props = {
  idenity: string,
  className: string,
  points: string,
  startColor: string,
  endColor: string,
  height: number
};

class CustomTriangle extends Component<Props> {
  static defaultProps = {
    idenity: "customFill",
    className: ""
  };

  render() {
    const { points, startColor, endColor, height, ...props } = this.props;
    return (
      <div {...props} className={`${props.className} custom-gradient`}>
        <svg
          width="100%"
          height={height}
          viewBox="0 0 100 100"
          preserveAspectRatio="none"
        >
          <defs>
            <linearGradient
              id={`${this.props.idenity}`}
              x1="0%"
              y1="0%"
              x2="100%"
              y2="0%"
            >
              <stop
                offset="0%"
                style={{ stopColor: startColor, stopOpacity: 1 }}
              />
              <stop
                offset="100%"
                style={{ stopColor: endColor, stopOpacity: 1 }}
              />
            </linearGradient>
          </defs>
          <polygon points={points} fill={`url(#${this.props.idenity})`} />
        </svg>
      </div>
    );
  }
}

export default CustomTriangle;
