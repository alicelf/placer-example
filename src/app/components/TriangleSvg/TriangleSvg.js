import React, { Component, Fragment } from "react";
import TopPartial from "./TopPartial";
import BottomPartial from "./BottomPartial";
import CustomTriangle from "./CustomTriangle";
import "./TriangleSvg.css";

class TriangleSvg extends Component {
  static Top = props => <TopPartial {...props} />;

  static Bottom = props => <BottomPartial {...props} />;

  static Custom = props => <CustomTriangle {...props} />;

  render() {
    return <Fragment>{this.props.children}</Fragment>;
  }
}

export default TriangleSvg;
