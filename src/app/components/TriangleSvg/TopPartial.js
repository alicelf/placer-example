import React, { Component } from "react";

class TopPartial extends Component {
  static defaultProps = {
    className: ""
  };

  render() {
    return (
      <div
        {...this.props}
        className={`${this.props.className} topPartGradient`}
      >
        <svg
          width="100%"
          height="120"
          viewBox="0 0 100 100"
          preserveAspectRatio="none"
        >
          <defs>
            <linearGradient
              id="topTriangleGradient"
              x1="0%"
              y1="0%"
              x2="100%"
              y2="0%"
              gradientUnits="userSpaceOnUse"
            >
              <stop
                offset="0%"
                style={{ stopColor: "#5e63e4", stopOpacity: 1 }}
              />
              <stop
                offset="100%"
                style={{ stopColor: "#6c79e9", stopOpacity: 1 }}
              />
            </linearGradient>
          </defs>
          <polygon
            points="0,0 200,170 0,200"
            fill="url(#topTriangleGradient)"
          />
        </svg>
      </div>
    );
  }
}

export default TopPartial;
