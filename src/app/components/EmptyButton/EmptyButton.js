import React, { Component } from "react";
import ButtonBase from "@material-ui/core/ButtonBase";
import "./EmptyButton.css";

type Props = {
  href?: string,
  classes?: string,
  onClick?: () => void,
  children?: React.Node | React.ChildrenArray<React.Node>
};

class EmptyButton extends Component<Props> {
  static defaultProps = {
    href: null,
    classes: "",
    children: []
  };

  render() {
    return (
      <ButtonBase
        classes={{
          root: `button-without-bg ${this.props.classes}`
        }}
        href={this.props.href}
      >
        {this.props.children}
      </ButtonBase>
    );
  }
}

export default EmptyButton;
