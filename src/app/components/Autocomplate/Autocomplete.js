import React, {Component} from 'react';
import './Autocomplete.css';

import {
  Popper,
  Fade,
  Paper,
  ClickAwayListener,
  MenuList,
  MenuItem,
  ListItemText,
  ListItemSecondaryAction,
  Button
} from '@material-ui/core';

import API from 'app/services/API/API';
import Place from 'app/models/Place';
import GooglePlace from 'app/models/GooglePlace';
import GoogleMapLoader from 'app/services/GoogleMapLoader/GoogleMapLoader';

const MAX_RESULT = 4;

export default class Autocomplete extends Component {

  static defaultProps = {
    className: '',
    value: '',
    delay: 10,
    onItemSelect: () => false
  };

  constructor(props) {
    super(props);

    this.state = {
      value: this.props.value,
      open: false,
      selectedIndex: -1,
      list: []
    };

    this.timerID = null;
    this.googlePLaceService = null;
    this.googleGeocoderService = null;
    this.googlePLaceOK = null;

    this.initGoogleServices();
  }

  initGoogleServices() {
    GoogleMapLoader.load(process.env.REACT_APP_GOOGLE_KEY).then((maps) => {
      this.googlePLaceService = new maps.places.AutocompleteService();
      this.googlePLaceOK = maps.places.PlacesServiceStatus.OK;
      this.googleGeocoderService = new maps.Geocoder();
    });
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.state.value) {
      this.setState({
        value: nextProps.value
      });

      this.querySearch(nextProps.value)
          .then(() => this.setState({open: true}));
    }
  }

  componentWillUnmount() {
    window.clearTimeout(this.timerID);
    this.googlePLaceService = null;
    this.googleGeocoderService = null;
  }

  querySearch(value) {
    return API.search(value).then((result) => {
      if (result.length) {
        this.setState({
          list: result.map((place) => {
            switch(place.info.type) {
              case 'venue':
                place.collection = 'venues';
                break;
              case 'complex':
                place.collection = 'complexes';
                break;
              default:
                throw Error('Unknown type');
            }

            return new Place(place);
          })
        });
      } else {
        return this.googleSearch(value).then((result) => {
          if (result.length) {
            this.setState({
              list: result.map((place) => new GooglePlace(place))
            });
          } else {
            this.setState({
              list: []
            });
          }
        });
      }
    });
  }

  googleSearch(inputText) {
    if (!this.googlePLaceService || !this.googleGeocoderService || !inputText) return;

    return new Promise((resolve) => {
      this.googlePLaceService.getPlacePredictions({
        input: inputText,
        componentRestrictions: {country: 'us'},
        types: ['establishment']
      }, (predictions, status) => {
        if (status !== this.googlePLaceOK) {
          resolve([]);
        }
        else {
          predictions.length = Math.min(MAX_RESULT, predictions.length);

          let geocodePromises = predictions.map((prediction) => {
            return new Promise((geoResolve) => {
              this.googleGeocoderService.geocode({
                'placeId': prediction.place_id
              }, (result) => {
                if (result.length && result[0]) {
                  result[0].name = prediction.structured_formatting.main_text;
                  geoResolve(result[0]);
                }
              });
            });
          });

          Promise.all(geocodePromises).then(places => resolve(places), () => resolve([]));
        }
      });
    });
  }

  onChangeHandler = (event) => {
    window.clearTimeout(this.timerID);
    this.setState({
      value: event.target.value,
      open: true
    });

    this.timerID = window.setTimeout(this.querySearch.bind(this, event.target.value), this.props.delay);
  };

  handleKeyDown = (event) => {
    switch (event.keyCode) {
      case 40:
        if (this.state.open) {
          this.setState((prevState) => {
            return {
              selectedIndex: Math.min(++prevState.selectedIndex, prevState.list.length - 1)
            }
          });
        }
        break;
      case 38:
        if (this.state.open) {
          this.setState((prevState) => {
            return {
              selectedIndex: Math.max(--prevState.selectedIndex, 0)
            }
          });
        }
        break;
      case 13:
        if (this.state.open) {
          this.chooseMenuItem(this.state.selectedIndex);
        } else {
          this.handleOpen();
        }
        break;
      case 27:
        if (this.state.open) {
          this.handleClose();
        }
        break;
      default:
        break;
    }
  };

  handleOpen = () => {
    if (this.state.list.length) {
      this.setState({ open: true });
    }
    this.querySearch(this.state.value)
        .then(() => this.setState({ open: true }));
  };

  handleClose = () => {
    this.setState({
      open: false,
      selectedIndex: -1
    });
  };

  chooseMenuItem(index) {
    if (!this.state.list[index]) return;

    this.handleClose();

    this.setState({
      value: this.state.list[index].name
    });

    this.props.onItemSelect(this.state.list[index]);
  }

  clickNotFoundItem = () => {
    console.log('NOT FOUND CLICKED ', this.state.value);
  };

  render() {

    return (
        <section className={`pl-autocomplete-component ${this.props.className}`}>
          <input type="search"
                 ref={(input) => this.inputEl = input}
                 placeholder="Search for any venue in the US"
                 value={this.state.value}
                 onClick={this.handleOpen}
                 onInput={this.onChangeHandler}
                 onKeyDown={this.handleKeyDown}
                 aria-owns={this.state.open ? 'pl-autocomplete-grow' : null}
                 aria-haspopup="true"
                 className={`pl-autocomplete-input ${(this.state.open? 'pl-autocomplete-input-open' : '')}`}
          />
          <Popper open={this.state.open} anchorEl={this.inputEl}
                  transition disablePortal
                  modifiers={{flip: {behavior: ['bottom']}}}>
            {({ TransitionProps }) => (
                <Fade {...TransitionProps} id="pl-autocomplete-grow"
                      classes={{root: 'pl-autocomplete-popper', elevation2: 'pl-autocomplete-paper-shadow'}}>
                  <Paper style={{width: this.inputEl.clientWidth}}>
                    <ClickAwayListener onClickAway={this.handleClose}>
                      <MenuList onClick={this.handleClose} classes={{root: 'pl-autocomplete-list'}} disablePadding>
                        {this.state.list.map((item, index) => (
                            <MenuItem key={item.id}
                                      classes={{root: 'pl-autocomplete-item'}}
                                      selected={index === this.state.selectedIndex}
                                      onClick={this.chooseMenuItem.bind(this, index)}>
                              <ListItemText primary={item.name}
                                            secondary={item.fullAddress}
                                            classes={{
                                              primary: 'pl-autocomplete-item-primary',
                                              secondary: 'pl-autocomplete-item-secondary'
                                            }}/>
                            </MenuItem>
                        ))}
                        {this.state.list.length === 0 && (
                            <MenuItem key="not-found-item"
                                      classes={{root: 'pl-autocomplete-item'}}
                                      onClick={this.clickNotFoundItem}>
                              <ListItemText primary="Can’t find a venue?"
                                            secondary="Let's find it together"
                                            classes={{secondary: 'pl-autocomplete-item-secondary'}}/>
                              <ListItemSecondaryAction>
                                <Button onClick={this.clickNotFoundItem} color="primary">Let us know</Button>
                              </ListItemSecondaryAction>
                            </MenuItem>
                        )}
                      </MenuList>
                    </ClickAwayListener>
                  </Paper>
                </Fade>
            )}
          </Popper>

          {this.props.children}
        </section>
    );
  }
}
