import React, { Component } from 'react';
import './Link.css';

import { Button } from '@material-ui/core';

export default class Link extends Component {

  render() {
    return (
        <Button {...this.props} classes={{root: 'pl-link-component'}} disableTouchRipple>
          {this.props.children}
        </Button>
    );
  }
}
