import React, { Component } from 'react';
import './InnerContent.css';

export default class InnerContent extends Component {

  static defaultProps = {
    className: ''
  };

  render() {
    return (
        <section {...this.props} className={`pl-inner-content ${this.props.className}`}>
          {this.props.children}
        </section>
    );
  }
}
