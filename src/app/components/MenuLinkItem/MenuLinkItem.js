import React, { Component } from "react";
import { Link } from "react-router-dom";

import "./MenuLinkItem.css";

export default class MenuLinkItem extends Component {
  static defaultProps = {
    className: "",
    external: false
  };

  render() {
    let { external, ...other } = this.props;

    return (
      <li
        {...other}
        className={`menu-link-item-component ${this.props.className}`}
      >
        {external ? (
          <a href={other.to} className="menu-link-item" target={other.target}>
            {other.children}
          </a>
        ) : (
          <Link to={other.to} className="menu-link-item">
            {other.children}
          </Link>
        )}
      </li>
    );
  }
}
