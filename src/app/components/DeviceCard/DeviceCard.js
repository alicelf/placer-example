import React, { Component } from "react";
import SubTitle from "../SubTitle/SubTitle";
import "./DeviceCard.css";

type Props = {
  alt: string,
  icon: string,
  title: string,
  subtitle: string
};

class DeviceCard extends Component<Props> {
  render() {
    const { icon, title, subtitle, alt } = this.props;
    return (
      <figure className="device-card-item">
        <img src={icon} alt={alt || "img-figure"} />
        <figcaption>
          <SubTitle>{title}</SubTitle>
          <p>{subtitle}</p>
        </figcaption>
      </figure>
    );
  }
}

export default DeviceCard;
