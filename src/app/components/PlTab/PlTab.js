import React, { Component } from "react";
import ButtonBase from "@material-ui/core/ButtonBase";
import withContext from "app/hoc/withContext";
import WithClasses from "app/hoc/WithClasses";
import { CSSTransition } from "react-transition-group";
import "./PlTab.css";

const { Provider, Consumer } = React.createContext();

const panelWrap = withContext(Consumer);

type Props = {
  defaultActive: string
};

type State = {
  activeChildren: string
};

export default class CustomTabs extends Component<Props, State> {
  static defaultProps = {
    className: ""
  };

  static Tab = panelWrap(
    ({ activeChildren, setActiveChildren, panelId, children }) => (
      <WithClasses
        classes={[
          "pl-custom-tab",
          activeChildren === panelId ? `is-active` : null
        ]}
      >
        <ButtonBase
          focusRipple
          onClick={() => setActiveChildren(panelId)}
          disableTouchRipple
        >
          {children}
        </ButtonBase>
      </WithClasses>
    )
  );

  // or activeChildren === panelId
  static Panel = panelWrap(({ activeChildren, children, panelId }) => {
    return (
      <CSSTransition
        in={activeChildren === panelId}
        timeout={300}
        classNames="panel"
        unmountOnExit
        key={panelId}
      >
        <div>{children}</div>
      </CSSTransition>
    );
  });

  constructor(props: Props) {
    super(props);
    this.state = {
      activeChildren: this.props.defaultActive
    };
  }

  setActiveChildren = (panelId: number | string) => {
    this.setState({
      activeChildren: panelId
    });
  };

  getContext() {
    return {
      activeChildren: this.state.activeChildren,
      setActiveChildren: this.setActiveChildren
    };
  }

  render() {
    return <Provider value={this.getContext()}>{this.props.children}</Provider>;
  }
}
