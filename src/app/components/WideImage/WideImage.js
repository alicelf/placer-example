import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import InnerContent from "app/components/InnerContent/InnerContent";
import "./WideImage.css";

class WideImage extends Component {
  render() {
    return (
      <InnerContent>
        <Grid container spacing={0} justify="flex-end">
          <div>
            <img className="wide-img" src={this.props.image} alt="@" />
          </div>
        </Grid>
      </InnerContent>
    );
  }
}

export default WideImage;
