import React, { Component } from 'react';
import './Pin.css';

export default class Pin extends Component {

  static defaultProps = {
    className: '',
    color: '#EF5771',
    width: 20,
    height: 20
  };

  render() {
    return (
        <svg {...this.props}
             xmlns="http://www.w3.org/2000/svg"
             width={this.props.width}
             className={`pl-pin ${this.props.className}`}
             height={this.props.height} viewBox="0 0 20 20">
          <g fill="none" fillRule="evenodd">
            <circle cx="10" cy="10" r="6.667" fill={this.props.color}/>
            <circle cx="10" cy="10" r="10" fill={this.props.color} fillOpacity=".3"/>
          </g>
        </svg>
    );
  }
}
