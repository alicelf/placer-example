import React, { Component } from 'react';
import './Label.css';

export default class Label extends Component {

  static defaultProps = {
    className: ''
  };

  render() {
    return (
        <div className={`pl-label-component ${this.props.className}`}>
          {this.props.children}
        </div>
    );
  }
}
