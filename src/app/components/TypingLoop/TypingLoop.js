import React, {Component} from 'react';
import './TypingLoop.css';

class Words {

  constructor(words = []) {
    this.currentIndex = -1;
    this.wordsList = words;
  };

  next() {
    if (++this.currentIndex >= this.wordsList.length) {
      this.currentIndex = 0;
    }

    return this.wordsList[this.currentIndex];
  };
}

export class Word extends Component {}

export default class TypingLoop extends Component {

  static defaultProps = {
    className: '',
    typingSpeed: 75,
    startDelay: 200,
    endDelay: 2000
  };

  constructor(props) {
    super(props);

    this.state = {
      input: ''
    };

    this.timerID = null;
    this.mounted = null;
  };

  componentDidMount() {
    this.mounted = true;
    this.words = new Words(this.convertWordsToArray(this.props.children));
    this.loop(this.words.next());
  };

  componentWillUnmount() {
    window.clearInterval(this.timerID);
    this.mounted = false;
    this.words = null;
  };

  convertWordsToArray(children) {
    return children.filter(child => child.type.name === Word.name).map(child => child.props.children);
  };

  delay(timeout) {
    return new Promise((resolve) => {
      window.setTimeout(resolve, timeout);
    });
  };

  loop(word) {
    if (!word) return;

    this.mounted && this.delay(this.props.startDelay).then(() => {
      this.mounted && this.forward(word).then((word) => {
        this.mounted && this.delay(this.props.endDelay).then(() => {
          this.mounted && this.backward(word).then(() => this.words && this.loop(this.words.next()));
        });
      });
    });
  };

  forward(word) {
    return new Promise((resolve) => {
      let counter = -1;

      this.timerID = window.setInterval(() => {
        this.setInput(word.substr(0, ++counter + 1));

        if (counter + 1 >= word.length) {
          window.clearInterval(this.timerID);
          resolve(word);
        }
      }, this.props.typingSpeed);
    });
  };

  backward(word) {
    return new Promise((resolve) => {
      let counter = word.length;

      this.timerID = window.setInterval(() => {
        this.setInput(word.substr(0, --counter));

        if (counter <= 0) {
          window.clearInterval(this.timerID);
          resolve(word);
        }
      }, this.props.typingSpeed);
    });
  };

  setInput(input) {
    this.setState({
      input
    });
  };

  render() {
    return (
        <span className={`pl-typing-loop ${this.props.className}`}>
          {this.state.input}
        </span>
    );
  };
}
