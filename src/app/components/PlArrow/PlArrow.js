import React, { Component } from "react";
import "./PlArrow.css";

type Props = {
  className: string
};

class PlArrow extends Component<Props> {
  static defaultProps = {
    className: ""
  };

  render() {
    return (
      <span className={`pl-arrow ${this.props.className}`}>
        <span className="bar" />
      </span>
    );
  }
}

export default PlArrow;
