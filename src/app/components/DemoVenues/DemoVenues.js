import React, {Component, Fragment} from 'react';
import { Button } from '@material-ui/core';
import './DemoVenues.css';

import Marker from '../GoogleMap/Marker/Marker';
import Pin from '../Pin/Pin';
import Label from '../Label/Label';
import Popup from '../GoogleMap/Popup/Popup';
import locationIcon from 'assets/main/location-icon.svg';

export default class DemoVenues extends Component {

  static defaultProps = {
    className: '',
    venues: []
  };

  static getDerivedStateFromProps(props) {
    return {
      markers: props.venues
    }
  };

  constructor(props) {
    super(props);

    this.state = {
      popupInfo: null
    };
  };

  onMouseTriggerHandler(index) {
    this.setState((prevState) => {
      prevState.markers[index].isHover = !prevState.markers[index].isHover;
      return prevState;
    });
  };

  openPopup(marker) {
    this.setState({popupInfo: marker});
  };

  closePopup() {
    this.setState({popupInfo: null});
  };

  render() {
    let {markers, popupInfo} = this.state;

    return (
        <Fragment>
          {markers.map((marker, index) => (
              <Marker key={marker.id}
                      latitude={marker.latitude}
                      longitude={marker.longitude}
                      className="pl-demo-market"
                      onMouseEnter={this.onMouseTriggerHandler.bind(this, index)}
                      onMouseLeave={this.onMouseTriggerHandler.bind(this, index)}
                      onClick={this.openPopup.bind(this, marker)}>
                <Pin className="pl-demo-pin"
                     width={marker.isHover? 30 : 20}
                     height={marker.isHover? 30 : 20}
                     color={marker.isHover? '#676DFD' : '#EF5771'}/>
                <Label className="pl-demo-label">{marker.name}</Label>
              </Marker>
          ))}
          {popupInfo
            && <Popup latitude={popupInfo.latitude}
                      longitude={popupInfo.longitude}
                      onClose={this.closePopup.bind(this)}
                      className="pl-demo-popup">
              <h4 className='pl-demo-popup-name'>{popupInfo.name}</h4>
              <div className='pl-demo-popup-address'>
                <img src={locationIcon} alt="location icon" className="pl-demo-popup-icon"/>
                <span>{popupInfo.address}</span>
              </div>
              <div className='pl-demo-popup-visits'>
                <span>Monthly Avg. Visits</span>
                <span>{popupInfo.visits}</span>
              </div>
              <Button variant="contained" color="primary"
                      href={popupInfo.href()} target="_blank">See Full Report</Button>
            </Popup>}
        </Fragment>
    );
  };
}
