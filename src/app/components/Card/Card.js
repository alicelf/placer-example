import React, { Component } from "react";
import "./Card.css";

class Card extends Component {
  render() {
    return (
      <div className="card-item">
        <div className="card-item-content">
          <div className="addons top" />
          <div className="children">{this.props.children}</div>
          <div className="addons bottom" />
        </div>
      </div>
    );
  }
}

export default Card;
