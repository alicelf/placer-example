import React, { Component, Fragment } from "react";
import "./FullScreen.css";

type Props = {
  bgImage: string,
  mobBg: string,
  styles: Object,
  className: string,
  children?: React.Node | React.ChildrenArray<React.Node>
};

class FullScreen extends Component<Props> {
  static defaultProps = {
    className: "",
    bgImage: "",
    mobBg: ""
  };

  render() {
    const { bgImage, mobBg, styles } = this.props;
    const bg = bgImage.length ? { backgroundImage: `url(${bgImage})` } : "";
    const mBg = mobBg.length ? { backgroundImage: `url(${mobBg})` } : "";

    return (
      <Fragment>
        <div
          style={{ ...bg, ...styles }}
          className={`full-screen-container hidden-on-phones ${
            this.props.className
          }`}
        >
          {this.props.children}
        </div>
        <div
          style={{ ...mBg, ...styles }}
          className={`full-screen-container visible-on-phones-only ${
            this.props.className
          }`}
        >
          {this.props.children}
        </div>
      </Fragment>
    );
  }
}

export default FullScreen;
