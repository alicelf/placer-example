import React, { Component } from "react";
import "./KeyCard.css";

class KeyCard extends Component {
  render() {
    return <div className="key-card">{this.props.children}</div>;
  }
}

export default KeyCard;
