import React, { Component } from 'react';
import './MainTitle.css';

export default class MainTitle extends Component {

  static defaultProps = {
    className: ''
  };

  render() {
    return (
        <h1 className={`pl-title ${this.props.className}`}>
          {this.props.children}
        </h1>
    );
  }
}
