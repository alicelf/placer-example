import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import InnerContent from "app/components/InnerContent/InnerContent";
import "./TitleWithButton.css";

class TitleWithButton extends Component {
  render() {
    const { children } = this.props;
    return (
      <InnerContent className="title-with-buttons-section">
        <div className="title-handler">{children}</div>
        <div className="button-handler">
          <Button
            href="http://..."
            variant="contained"
            color="primary"
            classes={{ root: "title-button" }}
          >
            Free Sign Up
          </Button>
        </div>
      </InnerContent>
    );
  }
}

export default TitleWithButton;
