import React, { Component } from "react";
import { matchPath } from "react-router-dom";
import { MenuItem, Button } from "@material-ui/core";
import { Link } from "react-router-dom";

import "./Navigation.css";
import Dropdown from "../Dropdown/Dropdown";
import MenuLinkItem from "../MenuLinkItem/MenuLinkItem";

const tags = {
  PRODUCTS: "products",
  SOLUTIONS: "solutions",
  PRICING: "pricing",
  COMPANY: "company",
  RETAIL: "retail",
  RESOURCES: "resources"
};

export default class Navigation extends Component {

  static getDerivedStateFromProps(props) {
    let match = matchPath(props.location.pathname, "/:tag");

    return {
      tag: match ? match.params.tag : null
    }
  };

  constructor(props) {
    super(props);

    this.state = {
      tag: null
    };
  }

  render() {
    return (
      <nav className="pl-navigation">
        <Dropdown
          label="Products"
          disableTouchRipple
          style={{ width: 92 }}
          classes={{button: `pl-navigation-dropdown-link ${this.state.tag === tags.PRODUCTS ? "pl-navigation-active" : ""}`, root: "pl-navigation-dropdown"}}
        >
          <MenuItem
            component={MenuLinkItem}
            to="/products/analytics"
          >
            Analytics
          </MenuItem>
          <MenuItem
            component={MenuLinkItem}
            to="/products/api"
          >
            API
          </MenuItem>
          <MenuItem
            component={MenuLinkItem}
            to="https://sdk.placer.io/"
            target="_blank"
            external
          >
            SDK
          </MenuItem>
        </Dropdown>
        <Dropdown
          label="Solutions"
          disableTouchRipple
          style={{ width: 95 }}
          classes={{button: `pl-navigation-dropdown-link ${this.state.tag === tags.SOLUTIONS ? "pl-navigation-active" : ""}`, root: "pl-navigation-dropdown"}}
        >
          <MenuItem
            component={MenuLinkItem}
            to="/solutions/shopping-centers"
          >
            Shopping Centers
          </MenuItem>
          <MenuItem
            component={MenuLinkItem}
            to="/solutions/cre"
          >
            Commercial Real Estate
          </MenuItem>
            <MenuItem
                component={MenuLinkItem}
                to="/solutions/retail"
            >
                Retail
            </MenuItem>
            <MenuItem
                component={MenuLinkItem}
                to="/solutions/hospitality"
            >
                Hospitality
            </MenuItem>
        </Dropdown>
        <Button
          component={Link}
          to="/pricing"
          classes={{ root: "pl-nav-button" }}
          disableTouchRipple
          style={{ width: 78 }}
          className={
            this.state.tag === tags.PRICING ? "pl-navigation-active" : ""
          }
        >
          Pricing
        </Button>
        <Dropdown
          label="Company"
          disableTouchRipple
          style={{ width: 96 }}
          classes={{button: `pl-navigation-dropdown-link ${this.state.tag === tags.COMPANY ? "pl-navigation-active" : ""}`, root: "pl-navigation-dropdown"}}
        >
          <MenuItem
            component={MenuLinkItem}
            to="/company/about"
          >
            About Us
          </MenuItem>
          <MenuItem
            component={MenuLinkItem}
            to="/company/team"
          >
              Our values
          </MenuItem>
            <MenuItem
                component={MenuLinkItem}
                to="/company/team"
            >
                We’re hiring!
            </MenuItem>
            <MenuItem
                component={MenuLinkItem}
                to="/company/team"
            >
                Contact Us
            </MenuItem>
        </Dropdown>
        <Dropdown
          label="Resources"
          disableTouchRipple
          style={{ width: 102 }}
          classes={{button: `pl-navigation-dropdown-link ${this.state.tag === tags.RESOURCES ? "pl-navigation-active" : ""}`, root: "pl-navigation-dropdown"}}
        >
          <MenuItem
            component={MenuLinkItem}
            to="/resources/our-data"
          >
            Our Data
          </MenuItem>
          <MenuItem
            component={MenuLinkItem}
            to="/resources/library"
          >
            Library
          </MenuItem>
            <MenuItem
                component={MenuLinkItem}
                to="https://blog.placer.io/"
                target="_blank"
                external
            >
                Blog
            </MenuItem>
        </Dropdown>
      </nav>
    );
  }
}
