import React from "react";

const withContext = ContextConsumer => Component => props => (
  <ContextConsumer>
    {context => <Component {...props} {...context} />}
  </ContextConsumer>
);

export default withContext;
