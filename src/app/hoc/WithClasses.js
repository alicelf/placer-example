import React from "react";

type Props = {
  classes: Array<string>,
  children?: React.Node | React.ChildrenArray<React.Node>
};

const WithClasses = (props: Props) => {
  const { classes } = props;
  return <span className={`${classes.join(" ")}`}>{props.children}</span>;
};

export default WithClasses;
