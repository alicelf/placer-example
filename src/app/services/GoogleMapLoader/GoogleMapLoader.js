export default class GoogleMapLoader {

  static promise = null;

  static load(key, options) {
    if (!GoogleMapLoader.promise) {
      GoogleMapLoader.promise = new Promise((resolve, reject) => {
        let script = GoogleMapLoader.createScriptTag(key, options);
        script.onload = () => resolve(window.google.maps);
        script.onerror = () => reject(new Error('Failed to load google map'));
      });
    }

    return GoogleMapLoader.promise;
  }

  static createScriptTag(key, options = {}) {
    let head = document.head || document.getElementsByTagName('head')[0];
    let script = document.createElement('SCRIPT');
    let URL = `https://maps.googleapis.com/maps/api/js?libraries=places${key? '&key=' + key : ''}`;

    script.type = options.type || 'text/javascript';
    script.charset = options.charset || 'utf8';
    script.async = options.async;
    script.src = URL;

    head.appendChild(script);

    return script;
  }
}