import GoogleMapLoader from './GoogleMapLoader';

it('get google map', () => {
  expect.assertions(1);
  return GoogleMapLoader.load().then((map) => {
    expect(map).toBeDefined();
  });
});
