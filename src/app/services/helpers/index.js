export default class PlHelper {
  /**
   * Make it scoped with prefix when you call it
   * @example theId = const theId = PlHelper.makeId(); theId("somepref")
   * @returns Incremented value with|withot prefix
   */
  static makeId = function() {
    let iterator = 0;
    // @param  {string|number=""} prefix?
    return function(prefix?: string | number = "") {
      return `${prefix}-${iterator++}`;
    };
  };
}
