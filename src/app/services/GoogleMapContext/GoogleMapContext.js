import React from 'react';

const GoogleMapContext = React.createContext(null);

export default GoogleMapContext;