export default class API {

  static buildUrl(url, params) {
    let urlPrams = Object.keys(params).map((key) => {
      return encodeURIComponent(key) + '=' + encodeURIComponent(params[key]);
    }).join('&');

    return process.env.REACT_APP_END_POINT + '/' + url + '?' + urlPrams;
  }

  static fetch(url, options = {}) {
    return fetch(url, {
      method: options.method || "GET",
      mode: options.mode || "cors",
      cache: options.cache || "no-cache",
      headers: options.headers || {
        "Content-Type": "application/json; charset=utf-8"
      },
      data: options.data? JSON.stringify(options.data) : null
    });
  };

  static fetchDemo() {
    return API.fetch(API.buildUrl('2/guru/search', {
      purchased_only: true,
      query: ''
    }), {
      cache: 'force-cache'
    }).then((response) => response.json())
      .then((json) => json.data);
  };

  static search(inputText) {
    return API.fetch(API.buildUrl('2/guru/search', {
      type: encodeURIComponent(['venues', 'complexes']),
      purchase_only: false,
      query: inputText,
      limit: 4,
    }), {
      cache: 'force-cache'
    }).then((response) => response.json())
       .then((json) => json.data);
  };
}