import React, { Component, Fragment } from "react";
import TitleWithButton from "app/components/TitleWithButton/TitleWithButton";
import WideImage from "app/components/WideImage/WideImage";
import SubTitle from "app/components/SubTitle/SubTitle";
import { prodPic } from "assets";

class AccessData extends Component {
  render() {
    return (
      <Fragment>
        <TitleWithButton>
          <SubTitle>
              Access Real-Time Data & Get Results Immediately
          </SubTitle>
        </TitleWithButton>

        <WideImage image={prodPic} />
      </Fragment>
    );
  }
}

export default AccessData;
