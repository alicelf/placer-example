// @flow
import React, { Component } from "react";
import IconButton from "@material-ui/core/Button";
import MobileStepper from "@material-ui/core/MobileStepper";
import SwipeableViews from "react-swipeable-views";
import "./TestimonialsSlider.css";
import data from "./data.json";

type Props = {
  className: string,
  theme: Object
};

type State = {
  activeStep: number
};

export default class TestimonialsSlider extends Component<Props, State> {
  static defaultProps = {
    className: ""
  };

  constructor(props: Props) {
    super(props);

    this.state = {
      activeStep: 0
    };
  }

  handleNext = () => {
    this.setState(prevState => {
      const nextStep = prevState.activeStep + 1;

      return {
        activeStep: nextStep === data.length ? 0 : nextStep
      };
    });
  };

  handleBack = () => {
    this.setState(prevState => {
      const prevStep = prevState.activeStep - 1;
      return {
        activeStep: prevStep === -1 ? data.length - 1 : prevStep
      };
    });
  };

  handleStepChange = (activeStep: number) => {
    this.setState({ activeStep });
  };

  onSwitching = (fromTo: number, type: string) => {
    if (type !== "end") {
      return true;
    }

    if (fromTo === 0) {
      this.setState({ activeStep: data.length - 1 });
    }

    if (fromTo === data.length - 1) {
      this.setState({ activeStep: 0 });
    }
  };

  render() {
    const { className } = this.props;
    const { activeStep } = this.state;
    const maxSteps = data.length;

    return (
      <div className={`slider-wrapper-view ${className}`}>
        <IconButton
          onClick={this.handleBack}
          classes={{ root: "navs" }}
          className="prev-nav"
          aria-label="Prev"
        >
          <span className="arrow" />
        </IconButton>

        <SwipeableViews
          index={this.state.activeStep}
          onChangeIndex={this.handleStepChange}
          enableMouseEvents
          onSwitching={this.onSwitching}
        >
          {data.map(step => (
            <div key={step.id} className="testimonials-single-view">
              <div className="content">“{step.content}”</div>
              <div className="author-scope">
                <img src={step.logo} alt="company-logo" />
                <div className="author-info">
                  <strong>{step.author}</strong>
                  <span>{step.post}</span>
                </div>
              </div>
            </div>
          ))}
        </SwipeableViews>

        <IconButton
          onClick={this.handleNext}
          classes={{ root: "navs" }}
          className="next-nav"
          aria-label="Next"
        >
          <span className="arrow" />
        </IconButton>

        <MobileStepper
          steps={maxSteps}
          position="static"
          activeStep={activeStep}
          classes={{ dot: "pl-slider-dot", dotActive: "pl-slider-dot-active" }}
          className="steps-view"
        />
      </div>
    );
  }
}
