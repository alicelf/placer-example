import React, { Component } from "react";
import SubTitle from "app/components/SubTitle/SubTitle";
import EmptyButton from "app/components/EmptyButton/EmptyButton";
import InnerContent from "app/components/InnerContent/InnerContent";
import "./PlacerData.css";

type Props = {
  iframeUrl: string
};

type State = {
  iframeHeight: number
};

class PlacerData extends Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      iframeHeight: 550
    };
  }

  componentDidMount() {
    this.onCalculateHeight();

    window.addEventListener("resize", this.onCalculateHeight);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.onCalculateHeight);
  }

  onCalculateHeight = () => {
    if (window.innerWidth < 426) {
      return this.setState({ iframeHeight: 200 });
    }

    if (window.innerWidth < 769) {
      return this.setState({ iframeHeight: 400 });
    }

    return this.setState({ iframeHeight: 550 });
  };

  render() {
    return (
      <InnerContent className="pd-content">
        <SubTitle>Data in Action</SubTitle>
        <p className="pd-subtitle">
          Visualize your customers' daily journey to discover new opportunities.
        </p>

        <iframe
          title="Placer data video"
          src={this.props.iframeUrl}
          width="100%"
          height={this.state.iframeHeight}
          frameBorder="0"
          allowFullScreen
        />

        <div className="pd-footer">
          <EmptyButton href="http://...">
            <span>More Videos</span> <i />
          </EmptyButton>
        </div>
      </InnerContent>
    );
  }
}

export default PlacerData;
