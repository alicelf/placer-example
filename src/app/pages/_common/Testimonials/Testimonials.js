import React, { Component } from "react";
import SubTitle from "app/components/SubTitle/SubTitle";
import TriangleSvg from "app/components/TriangleSvg/TriangleSvg";
import TestimonialsSlider from "../TestimonialsSlider/TestimonialsSlider";
import "./Testimonials.css";

class Testimonials extends Component {
  render() {
    return (
      <div className="testimonials-wrapper">
        <TriangleSvg>
          <TriangleSvg.Custom
            height={180}
            endColor="#f7f6f5"
            startColor="#f7f6f5"
            points="0,20 100,100 0,100"
            style={{ transform: "translateY(5px)" }}
          />
        </TriangleSvg>
        <div className="testimonials-section">
          <SubTitle>What Our Clients Say</SubTitle>

          <TestimonialsSlider className="testimonials-slider" />
        </div>
      </div>
    );
  }
}

export default Testimonials;
