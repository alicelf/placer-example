import React, { Component } from "react";
import "./ProductsList.css";

import SubTitle from "app/components/SubTitle/SubTitle";

export default class ProductsList extends Component {
  render() {
    return (
      <section className="pl-products-list-section">
        <SubTitle>Products</SubTitle>
      </section>
    );
  }
}
