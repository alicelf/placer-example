import React, { Component } from "react";
import Page from "app/components/Page/Page";
import ProductsList from "./sections/ProductsList/ProductsList";

export default class ProductsPage extends Component {
  render() {
    return (
      <Page>
        <ProductsList />
      </Page>
    );
  }
}
