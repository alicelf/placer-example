import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import InnerContent from "app/components/InnerContent/InnerContent";
import PriceTopTriangle from "./PriceTopTriangle";
import SubTitle from "app/components/SubTitle/SubTitle";
import PriceBottomTriangle from "./PriceBottomTriangle";
import PlList from "app/components/PlList/PlList";
import "./Prices.css";
import { apiIconVariant, reportIcon, sdkIconVariant } from "assets";
import data from "./data.json";

type Props = {};

class Prices extends Component<Props> {
  render() {
    return (
      <section className="prices-section-wrapper">
        <PriceTopTriangle />

        <div className="prices-container">
          <InnerContent className="price-cards">
            <div className="free-card single-card">
              <SubTitle>Free</SubTitle>
              <PlList listData={data.free} />
              <Button
                variant="contained"
                size="large"
                color="primary"
                className="pl-button-lg empty-button"
                href="http://..."
              >
                Sign Up
              </Button>
            </div>
            <div className="premium-card single-card">
              <SubTitle>
                Premium <span>Customize Your Package</span>
              </SubTitle>
              <div className="premium-inner">
                <div className="features-container">
                  <PlList listData={data.premium} />
                </div>

                <div className="additional-solutions">
                  <p>Additional Solutions:</p>
                  <ul className="solutions-list">
                    <li>
                      <img src={apiIconVariant} alt="api-icon" />
                      <span>Custom Reports</span>
                    </li>
                    <li>
                      <img src={reportIcon} alt="placer-api-icon" />
                      <span>Placer API</span>
                    </li>
                    <li>
                      <img src={sdkIconVariant} alt="placer-api-icon" />
                      <span>Placer SDK</span>
                    </li>
                  </ul>
                </div>
              </div>
              <Button
                variant="contained"
                size="large"
                color="primary"
                className="pl-button-lg"
                href="http://..."
              >
                Contact Us
              </Button>
            </div>

            <div className="single-card additional-solution-tablet-view">
              <p>Additional Solutions:</p>
              <ul className="solutions-list">
                <li>
                  <img src={apiIconVariant} alt="api-icon" />
                  <span>Custom Reports</span>
                </li>
                <li>
                  <img src={reportIcon} alt="placer-api-icon" />
                  <span>Placer API</span>
                </li>
                <li>
                  <img src={sdkIconVariant} alt="placer-api-icon" />
                  <span>Placer SDK</span>
                </li>
              </ul>
            </div>
          </InnerContent>
        </div>

        <PriceBottomTriangle />
      </section>
    );
  }
}

export default Prices;
