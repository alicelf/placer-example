import React, { Component } from "react";
import TriangleSvg from "app/components/TriangleSvg/TriangleSvg";

type Props = {};

class PriceBottomTriangle extends Component<Props> {
  render() {
    return (
      <TriangleSvg>
        <TriangleSvg.Custom
          idenity="prices-bottom-cut"
          height={180}
          endColor="#fff"
          startColor="#fff"
          points="0,0 100,80 100,100 0,100"
          style={{ marginTop: -150 }}
        />
      </TriangleSvg>
    );
  }
}

export default PriceBottomTriangle;
