import React, { Component } from "react";
import TriangleSvg from "app/components/TriangleSvg/TriangleSvg";

type Props = {};

class PriceTopTriangle extends Component<Props> {
  render() {
    return (
      <TriangleSvg>
        <TriangleSvg.Custom
          idenity="prices-top-cut"
          height={180}
          endColor="#fff"
          startColor="#fff"
          points="0,0 100,0 100,20 0,100"
          style={{ transform: "translateY(170px)" }}
        />
      </TriangleSvg>
    );
  }
}

export default PriceTopTriangle;
