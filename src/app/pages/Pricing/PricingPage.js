import React, { Component } from "react";
import Page from "app/components/Page/Page";
import Prices from "./sections/Prices/Prices";

type Props = {};

class PricingPage extends Component<Props> {
  render() {
    return (
      <Page>
        <Prices />
      </Page>
    );
  }
}

export default PricingPage;
