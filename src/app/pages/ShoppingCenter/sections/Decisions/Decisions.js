import React, { Component } from "react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import TriangleSvg from "app/components/TriangleSvg/TriangleSvg";
import SubTitle from "app/components/SubTitle/SubTitle";
import MiddleTitle from "app/components/MiddleTitle/MiddleTitle";
import PlHelper from "app/services/helpers";
import "./Decisions.css";
import { checkmark } from "assets";
import dataCards from "./dataCards.json";

const theId = PlHelper.makeId();

type Props = {};

class Decisions extends Component<Props> {
  render() {
    return (
      <div className="data-driven-container">
        <TriangleSvg>
          <TriangleSvg.Custom
            height={180}
            endColor="#f7f6f5"
            startColor="#f7f6f5"
            points="0,40 100,100 0,100"
            style={{ transform: "translateY(5px)" }}
            idenity="data-driven-triangle-top"
          />
        </TriangleSvg>

        <div className="data-driven-inner">
          <SubTitle>
            Data Driven Decisions Across all Business Functions
          </SubTitle>
          <div className="data-cards">
            {dataCards.map(item => (
              <Card key={item.title} className="the-card">
                <CardContent>
                  <img src={item.img} alt={`${item.title} `} />
                  <MiddleTitle>{item.title}</MiddleTitle>
                  <ul>
                    {item.list.map(listItem => (
                      <li key={theId("decisions")}>
                        <img src={checkmark} alt="checkmark" />
                        <span>{listItem}</span>
                      </li>
                    ))}
                  </ul>
                </CardContent>
              </Card>
            ))}
          </div>
        </div>

        <TriangleSvg>
          <TriangleSvg.Custom
            height={180}
            endColor="#f7f6f5"
            startColor="#f7f6f5"
            points="0,0 100,0 100,100"
            idenity="data-driven-triangle-bottom"
          />
        </TriangleSvg>
      </div>
    );
  }
}

export default Decisions;
