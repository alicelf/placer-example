import React, { Component } from "react";
import PlTab from "app/components/PlTab/PlTab";
import InnerContent from "app/components/InnerContent/InnerContent";
import data from "./data.json";

class DesktopTabs extends Component {
  render() {
    return (
      <InnerContent className="desktop-tabs-handler hidden-on-phones">
        <PlTab defaultActive="foot-traffic">
          <div className="desktop-tab-buttons">
            <PlTab.Tab panelId="foot-traffic">Foot Traffic</PlTab.Tab>
            <PlTab.Tab panelId="trade-area">Trade Area</PlTab.Tab>
            <PlTab.Tab panelId="audience">Audience</PlTab.Tab>
            <PlTab.Tab panelId="customer-journey">Customer Journey</PlTab.Tab>
            <PlTab.Tab panelId="portfolio">Portfolio</PlTab.Tab>
          </div>

          <div className="desktop-panels">
            {data.map(({ id, body, imgSrc }) => (
              <PlTab.Panel panelId={id} key={id}>
                <img src={imgSrc} alt={id} />
                <p>{body}</p>
              </PlTab.Panel>
            ))}
          </div>
        </PlTab>
      </InnerContent>
    );
  }
}

export default DesktopTabs;
