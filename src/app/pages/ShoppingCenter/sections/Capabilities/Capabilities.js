import React, { Component } from "react";
import SubTitle from "app/components/SubTitle/SubTitle";
import InnerContent from "app/components/InnerContent/InnerContent";
import DesktopTabs from "./DesktopTabs";
import TriangleSvg from "app/components/TriangleSvg/TriangleSvg";
import MobileAccordion from "./MobileAccordion";
import "./Capabilities.css";

class Capabilities extends Component {
  render() {
    return (
      <section className="capabilities-section">
        <InnerContent>
          <SubTitle>Capabilities</SubTitle>
        </InnerContent>

        <DesktopTabs />
        <MobileAccordion />
        <TriangleSvg>
          <TriangleSvg.Custom
            idenity="capabilities-bottom-triangle"
            height={180}
            endColor="#f7f6f5"
            startColor="#f7f6f5"
            points="0,20 100,95 100,100 0,100"
            style={{
              // transform: "translateY(7px)",
              position: "absolute",
              width: "100%",
              left: 0,
              bottom: -6
            }}
          />
        </TriangleSvg>
      </section>
    );
  }
}

export default Capabilities;
