import React, { Component } from "react";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import { plus, minus } from "assets";
import data from "./data.json";

class MobileAccordion extends Component {
  constructor(props) {
    super(props);

    this.state = {
      expanded: "foot-traffic"
    };
  }

  handleChange = panel => (event, expanded) => {
    this.setState({
      expanded: expanded ? panel : false
    });
  };

  render() {
    const { expanded } = this.state;

    return (
      <div className="mobile-capabilities-accordion visible-on-phones-only">
        {data.map(({ id, body, mobImg, title }) => {
          const exp = expanded === id;
          const expandIcon = (
            <span>
              <img src={exp ? minus : plus} alt="expanded" />
            </span>
          );
          return (
            <ExpansionPanel
              key={id}
              expanded={exp}
              onChange={this.handleChange(id)}
              classes={{
                root: `panel-container ${exp ? "panel-expanded" : ""}`
              }}
            >
              <ExpansionPanelSummary
                expandIcon={expandIcon}
                classes={{
                  root: "panel-heading",
                  expandIcon: "pl-expand-icon"
                }}
              >
                {title}
              </ExpansionPanelSummary>
              <ExpansionPanelDetails classes={{ root: "panel-content" }}>
                <img src={mobImg} alt={id} />
                <p>{body}</p>
              </ExpansionPanelDetails>
            </ExpansionPanel>
          );
        })}
      </div>
    );
  }
}

export default MobileAccordion;
