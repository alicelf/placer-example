import React, { Component } from "react";
import InnerContent from "app/components/InnerContent/InnerContent";
import SubTitle from "app/components/SubTitle/SubTitle";
import TriangleSvg from "app/components/TriangleSvg/TriangleSvg";
import "./Benefits.css";

type Props = {
  title?: string,
  children: React.Node | React.ChildrenArray<React.Node>
};

class Benefits extends Component<Props> {
  static defaultProps = {
    title: "Benefits"
  };

  render() {
    return (
      <section className="benefits-section">
        <InnerContent>
          <SubTitle>{this.props.title}</SubTitle>
        </InnerContent>
        <InnerContent className="data-section">
          {this.props.children}
        </InnerContent>
        <TriangleSvg>
          <TriangleSvg.Custom
            height={220}
            endColor="#fff"
            startColor="#fff"
            points="0,0 100,0 0,70"
            className="bottom-triangle"
            style={{ transform: "translateY(-5px)" }}
          />
        </TriangleSvg>
      </section>
    );
  }
}

export default Benefits;
