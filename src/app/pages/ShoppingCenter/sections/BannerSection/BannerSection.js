import React, { Component } from "react";
import ButtonBase from "@material-ui/core/ButtonBase";
import FullScreen from "app/components/FullScreen/FullScreen";
import InnerContent from "app/components/InnerContent/InnerContent";
import PlArrow from "app/components/PlArrow/PlArrow";
import { shoppingCenterBanner, reportPic, shoppingCenterMobile } from "assets";
import "./BannerSection.css";

class BannerSection extends Component {
  render() {
    return (
      <FullScreen
        className="banner-area-shopping"
        bgImage={shoppingCenterBanner}
        mobBg={shoppingCenterMobile}
      >
        <InnerContent>
          <h1>Foot-Traffic Market Intelligence for Shopping Centers</h1>
          <p>
            Attract high value customers, engage your ideal tenants and identify
            acquisition opportunities
          </p>
          <ButtonBase
            focusRipple
            classes={{
              root: "widget-sample"
            }}
          >
            <img src={reportPic} alt="reports-illustration" />
            <div>
              <p>
                <strong>
                  Sample Dashboard <PlArrow />
                </strong>
                <span>Westfield, 220 San Francisco</span>
              </p>
            </div>
          </ButtonBase>
        </InnerContent>
      </FullScreen>
    );
  }
}

export default BannerSection;
