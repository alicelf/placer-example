import React, { Component } from "react";
// components
import Page from "app/components/Page/Page";
import BannerSection from "./sections/BannerSection/BannerSection";
import AccessData from "app/pages/_common/AccessData/AccessData";
import Decisions from "./sections/Decisions/Decisions";
import Benefits from "./sections/Benefits/Benefits";
import beneData from "./sections/Benefits/data.json";
import Capabilities from "./sections/Capabilities/Capabilities";
import Testimonials from "app/pages/_common/Testimonials/Testimonials";
import PlacerData from "app/pages/_common/PlacerData/PlacerData";
import Button from "@material-ui/core/Button";
import "./ShoppingCenter.css";

class ShoppingCenterPage extends Component {
  render() {
    return (
      <Page className="shopping-center-page">
        <BannerSection />
        <AccessData />
        <Decisions />
        <Benefits>
          {beneData.map(item => (
            <article key={item.id}>
              <h3>{item.title}</h3>
              <p>{item.body}</p>
            </article>
          ))}
        </Benefits>
        <Capabilities />
        <Testimonials />
        <div className="placer-data-view">
          <PlacerData iframeUrl="https://player.vimeo.com/video/271255106" />

          <Button
            href="http://..."
            variant="contained"
            size="large"
            color="primary"
            classes={{
              root: "pl-button-lg",
              label: "inner-text"
            }}
          >
            Get Started
          </Button>
        </div>
      </Page>
    );
  }
}

export default ShoppingCenterPage;
