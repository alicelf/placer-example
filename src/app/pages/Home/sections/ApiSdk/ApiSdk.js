import React, { Component } from "react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import EmptyButton from "app/components/EmptyButton/EmptyButton";
import SubTitle from "app/components/SubTitle/SubTitle";
import InnerContent from "app/components/InnerContent/InnerContent";
import { apiIcon, sdkIcon } from "assets";
import "./ApiSdk.css";

class ApiSdk extends Component {
  render() {
    return (
      <div className="api-sdk-section">
        <SubTitle>Do More With Placer Data</SubTitle>

        <InnerContent className="api-sdk-wrapper">
          <Card className="api-sdk-card">
            <CardContent>
              <img src={apiIcon} alt="api-icon" />
              <SubTitle>API</SubTitle>
              <p>Build innovative products with Placer’s data</p>
              <EmptyButton href="http://...">
                <span>Contact Us</span> <i />
              </EmptyButton>
            </CardContent>
          </Card>

          <Card className="api-sdk-card">
            <CardContent>
              <img src={sdkIcon} alt="api-icon" />
              <SubTitle>SDK</SubTitle>
              <p>Integrate the Placer SDK</p>
              <EmptyButton href="http://...">
                <span>Get the SDK</span> <i />
              </EmptyButton>
            </CardContent>
          </Card>
        </InnerContent>
      </div>
    );
  }
}

export default ApiSdk;
