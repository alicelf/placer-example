import React, { Component } from "react";
import InnerContent from "app/components/InnerContent/InnerContent";
import SubTitle from "app/components/SubTitle/SubTitle";
import "./KeyInsights.css";

class KeyInsights extends Component {
  render() {
    const { title, children } = this.props;
    return (
      <div className="key-insights">
        <InnerContent className="key-title">
          <SubTitle>{title}</SubTitle>
        </InnerContent>
        <InnerContent className="key-insights-cards">{children}</InnerContent>
      </div>
    );
  }
}

export default KeyInsights;
