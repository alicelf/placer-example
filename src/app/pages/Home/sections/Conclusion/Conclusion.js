import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import SubTitle from "app/components/SubTitle/SubTitle";
import InnerContent from "app/components/InnerContent/InnerContent";
import "./Conclusion.css";

class Conclusion extends Component {
  render() {
    return (
      <div className="conclusion-section">
        <InnerContent>
          <SubTitle className="conclusion-heading">
              Learn more about how Placer can help grow your business
          </SubTitle>

          <Button
            href="http://..."
            variant="contained"
            color="primary"
            className="title-button"
            classes={{ label: "conclusion-button" }}
          >
            Get Started
          </Button>
        </InnerContent>
      </div>
    );
  }
}

export default Conclusion;
