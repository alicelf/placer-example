import React, { Component, Fragment } from "react";
import InnerContent from "app/components/InnerContent/InnerContent";
import MiddleTitle from "app/components/MiddleTitle/MiddleTitle";
import TriangleSvg from "app/components/TriangleSvg/TriangleSvg";
import "./Sponsors.css";

class Sponsors extends Component {
  render() {
    const { children } = this.props;
    return (
      <Fragment>
        <section className="sponsors-section">
          <InnerContent>
            <MiddleTitle>Loved and Trusted By</MiddleTitle>
            <div className="child-wrapper">{children}</div>
          </InnerContent>
        </section>
        <TriangleSvg>
          <TriangleSvg.Custom
            height={220}
            endColor="#f7f6f5"
            startColor="#f7f6f5"
            points="0,0 100,0 100,70 0,10"
          />
        </TriangleSvg>
      </Fragment>
    );
  }
}

export default Sponsors;
