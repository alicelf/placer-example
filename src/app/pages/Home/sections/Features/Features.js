import React, { Component } from "react";
import SubTitle from "app/components/SubTitle/SubTitle";
import InnerContent from "app/components/InnerContent/InnerContent";
import TriangleSvg from "app/components/TriangleSvg/TriangleSvg";
import "./Features.css";

class Features extends Component {
  render() {
    const { title, children } = this.props;
    return (
      <div className="features-wrapper">
        <TriangleSvg>
          <TriangleSvg.Top />
        </TriangleSvg>

        <section className="pl-features-section">
          <InnerContent className="white-title">
            <SubTitle>{title}</SubTitle>
          </InnerContent>

          <InnerContent className="cards">{children}</InnerContent>
        </section>

        <TriangleSvg>
          <TriangleSvg.Bottom />
        </TriangleSvg>
      </div>
    );
  }
}

export default Features;
