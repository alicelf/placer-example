import React, { Component, Fragment } from "react";
import EmptyButton from "app/components/EmptyButton/EmptyButton";
import InnerContent from "app/components/InnerContent/InnerContent";
import SubTitle from "app/components/SubTitle/SubTitle";
import TriangleSvg from "app/components/TriangleSvg/TriangleSvg";
import "./Devices.css";

class Devices extends Component {
  render() {
    const { children } = this.props;
    return (
      <Fragment>
        <TriangleSvg>
          <TriangleSvg.Custom
            height={180}
            endColor="#f7f6f5"
            startColor="#f7f6f5"
            points="0,100 100,0 100,100"
            style={{ transform: "translateY(5px)" }}
          />
        </TriangleSvg>

        <section className="devices-section">
          <InnerContent>
            <SubTitle className="devices-subtitle">
              Deployed into Millions of Devices
            </SubTitle>
          </InnerContent>
          <InnerContent className="child-wrapper">{children}</InnerContent>

          <InnerContent className="learn-more-footer">
            <EmptyButton href="http://...">
              <span>Learn How it Works</span> <i />
            </EmptyButton>
          </InnerContent>
        </section>

        <TriangleSvg>
          <TriangleSvg.Custom
            height={220}
            endColor="#f7f6f5"
            startColor="#f7f6f5"
            points="0,0 100,0 100,70 0,10"
          />
        </TriangleSvg>
      </Fragment>
    );
  }
}

export default Devices;
