import React, { Component } from 'react';

import './Main.css';

import InnerContent from 'app/components/InnerContent/InnerContent';
import GoogleMap from 'app/components/GoogleMap/GoogleMap';
import MainTitle from 'app/components/MainTitle/MainTitle';
import TypingLoop, { Word } from 'app/components/TypingLoop/TypingLoop';
import DemoVenues from 'app/components/DemoVenues/DemoVenues';
import Autocomplete from 'app/components/Autocomplate/Autocomplete';
import Link from 'app/components/Link/Link';
import API from 'app/services/API/API';
import Venue from 'app/models/Venue';
import Complex from 'app/models/Complex';

export default class Main extends Component {

  constructor(props) {
    super(props);

    this.state = {
      selectedText: '',
      demoVenues: [],
      mapOptions: {}
    };
  }

  componentDidMount() {
    this.onResize();
    this.fetchDemoVenues();

    window.addEventListener('resize', this.onResize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.onResize);
  }

  onResize = () => {
    if (window.innerWidth <= 767) {
      this.setState({
        mapOptions: {
          zoom: 13,
          center: {
            lng: -122.375274,
            lat: 37.652392
          }
        }
      });
    } else {
      this.setState({
        mapOptions: {
          zoom: 14,
          center: {
            lng: -122.438274,
            lat: 37.792392
          }
        }
      });
    }
  };

  changeInput = (selectedText) => {
    this.setState({ selectedText });
  };

  handleItemSelect = (item) => {
    window.open(item.href(), '_blank').focus();
  };

  fetchDemoVenues() {
    API.fetchDemo().then((data) => {
      this.setState({
        demoVenues: data.map((d) => {
          switch(d.info.type) {
            case 'venue':
              return new Venue(d);
            case 'complex':
              return new Complex(d);
            default:
              throw Error('Unknown type');
          }
        })
      });
    });
  };

  render() {
    return (
        <section className="pl-main-section">
          <GoogleMap className="pl-main-google-map"
                     options={this.state.mapOptions}>
            <DemoVenues venues={this.state.demoVenues}/>
          </GoogleMap>
          <InnerContent className="pl-main-section-inner-content">
            <div className="pl-main-section-left-block">
              <MainTitle className="pl-main-section-main-title">
                Get Insights for Any
                <TypingLoop className="pl-main-section-typing">
                  <Word>Casino</Word>
                  <Word>Retailer</Word>
                  <Word>Place</Word>
                  <Word>Theme Park</Word>
                  <Word>Mall</Word>
                  <Word>Stadium</Word>
                </TypingLoop>
              </MainTitle>
              <div className="pl-main-section-text">
                  Unprecedented visibility into consumer foot-traffic
              </div>
              <Autocomplete delay={500}
                            className="pl-main-section-autocomplete"
                            onItemSelect={this.handleItemSelect}
                            value={this.state.selectedText}/>
              <Link color="primary"
                    className="pl-main-section-example"
                    href={this.state.demoVenues[0] && this.state.demoVenues[0].href()}
                    target="_blank"
                    onClick={this.changeInput.bind(this, this.state.demoVenues[0] && this.state.demoVenues[0].name)}>
                Example: {this.state.demoVenues[0] && this.state.demoVenues[0].name}
              </Link>
            </div>
          </InnerContent>
        </section>
    );
  }
}
