import React, { Component } from "react";
import Button from "@material-ui/core/Button";
// components
import Page from "app/components/Page/Page";
import MiddleTitle from "app/components/MiddleTitle/MiddleTitle";
import Card from "app/components/Card/Card";
import KeyCard from "app/components/KeyCard/KeyCard";
import DeviceCard from "app/components/DeviceCard/DeviceCard";
// sections
import Main from "./sections/Main/Main";
import AccessData from "app/pages/_common/AccessData/AccessData";
import Features from "./sections/Features/Features";
import Sponsors from "./sections/Sponsors/Sponsors";
import KeyInsights from "./sections/KeyInsights/KeyInsights";
import Devices from "./sections/Devices/Devices";
import PlacerData from "app/pages/_common/PlacerData/PlacerData";
import ApiSdk from "./sections/ApiSdk/ApiSdk";
import Testimonials from "app/pages/_common/Testimonials/Testimonials";
import Conclusion from "./sections/Conclusion/Conclusion";
// images
import * as images from "assets";

type Props = {};

export default class HomePage extends Component<Props> {
  render() {
    return (
      <Page>
        <Main />
        <AccessData />
        <Features title="Succeed in the Retail Economy">
          <Card>
            <img src={images.compPic} alt="@" />
            <h3>Competitive Benchmarking</h3>
            <p>
              Compare your business performance with any local or distant
              competitor
            </p>
            <Button href="http://..." color="primary" className="card-button">
              Learn More
            </Button>
          </Card>

          <Card>
            <img src={images.storePic} alt="@" />
            <h3>Store Intelligence</h3>
            <p>
              Gain insight into + 13 M venues and places across the United Sates
            </p>
            <Button href="http://..." color="primary" className="card-button">
              Learn More
            </Button>
          </Card>

          <Card>
            <img src={images.industryPic} alt="@" />
            <h3>Industry Trends</h3>
            <p>
              Analyze foot - traffic and customer behavior in any retail
              industry
            </p>
            <Button href="http://..." color="primary" className="card-button">
              Learn More
            </Button>
          </Card>
        </Features>

        <Sponsors>
          <a
            rel="noopener noreferrer"
            href="https://www.caesars.com/"
            target="_blank"
          >
            <img src={images.caesar} alt="caesars-logo" />
          </a>
          <a
            rel="noopener noreferrer"
            href="https://www.newmarkmerrill.com/"
            target="_blank"
          >
            <img src={images.newmark} alt="newmark-logo" />
          </a>
          <a
            rel="noopener noreferrer"
            href="https://www.oath.com/"
            target="_blank"
          >
            <img src={images.oath} alt="oath-logo" />
          </a>
          <a
            rel="noopener noreferrer"
            href="https://www.redbox.com/"
            target="_blank"
          >
            <img src={images.redbox} alt="redbox-logo" />
          </a>
          <a
            rel="noopener noreferrer"
            href="https://www.beachboardwalk.com/"
            target="_blank"
          >
            <img src={images.boardwalk} alt="boardwalk-logo" />
          </a>
        </Sponsors>

        <KeyInsights title="Key Insights">
          <KeyCard>
            <img src={images.visits} alt="visits-icon" />
            <MiddleTitle>Visits</MiddleTitle>
            <p>Learn how many visitors by hour, day, and month</p>
          </KeyCard>
          <KeyCard>
            <img src={images.flowIcon} alt="flow-icon" />
            <MiddleTitle>Customer Journey</MiddleTitle>
            <p>Know where customers are coming from and going to</p>
          </KeyCard>
          <KeyCard>
            <img src={images.audienceIcon} alt="audience-icon" />
            <MiddleTitle>Audience</MiddleTitle>
            <p>Identify customer demographics, interests, and intent</p>
          </KeyCard>
          <KeyCard>
            <img src={images.tradeIcon} alt="trade-icon" />
            <MiddleTitle>Trade Area</MiddleTitle>
            <p>
              Discover the most important areas generating true economic
              activity
            </p>
          </KeyCard>
          <KeyCard>
            <img src={images.churnIcon} alt="churn-icon" />
            <MiddleTitle>Churn</MiddleTitle>
            <p>
              Track customer churn and understand evolving consumer patterns
            </p>
          </KeyCard>
          <KeyCard>
            <img src={images.eventsIcon} alt="events-icon" />
            <MiddleTitle>Special Events</MiddleTitle>
            <p>Understand how large events will impact business</p>
          </KeyCard>
        </KeyInsights>

        <Devices>
          <DeviceCard
            icon={images.visitorsIcon}
            title="1.5B+"
            subtitle="Monthly Visitors"
          />
          <DeviceCard
            icon={images.devicesIcon}
            title="20M+"
            subtitle="Active Devices"
          />
          <DeviceCard
            icon={images.venueIcon}
            title="13M+"
            alt="venues-icon"
            subtitle="Venues"
          />
          <DeviceCard
            icon={images.mobileIcon}
            title="500+"
            alt="mobile-icon"
            subtitle="Mobile Apps"
          />
        </Devices>

        <PlacerData iframeUrl="https://player.vimeo.com/video/271255106" />

        <ApiSdk />

        <Testimonials />

        <Conclusion />
      </Page>
    );
  }
}
