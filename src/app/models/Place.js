// @flow
export default class Place {

  constructor(data) {
    this.fullAddress = data.info.address.formatted_address || 'Nationwide';
    this.address = data.info.address.address;
    this.id = data.info.id;
    this.name = data.info.name;
    this.visits = Place.prettifyVisits(data && data.overview && data.overview.estimated_foottraffic);
    this.collection = data.collection;
  }

  static prettifyVisits(visits) {
    if (!visits) return 'N/A';

    if (visits > 1000000) {
      return (visits / 1000000).toFixed(2) + 'M';
    }

    if (visits > 1000) {
      return (visits / 1000).toFixed(1) + 'K';
    }
  }

  href() {
    return `${process.env.REACT_APP_END_POINT}/#!/admin/insights/${this.collection}/${this.id}/overview`
  }
}