import Place from './Place';

export default class Complex extends Place {
  constructor(data) {
    super(data);

    this.collection = 'complexes';
    this.polygons = this.createGooglePolygon(data.info.polygon[0]);

    this.latitude = this.findAvgLatitude(data.info.polygon[0]);
    this.longitude = this.findAvgLongitude(data.info.polygon[0]);
  }

  createGooglePolygon(polygon = []) {
    return polygon.map(point => {
      return {
        lng: point[0],
        lat: point[1]
      };
    });
  }

  findAvgLatitude(polygon = []) {
    polygon.sort((a, b) => {
      return a[1] - b[1];
    });

    return polygon[0][1] + (polygon[polygon.length - 1][1] - polygon[0][1]) / 2;
  }

  findAvgLongitude(polygon = []) {
    polygon.sort((a, b) => {
      return a[0] - b[0];
    });

    return polygon[0][0] + (polygon[polygon.length - 1][0] - polygon[0][0]) / 2;
  }
}