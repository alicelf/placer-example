export default class GooglePlace {

  constructor(data) {
    this.fullAddress = data.formatted_address;
    this.address = data.address_components[0].long_name;
    this.id = data.place_id;
    this.name = data.name;

    this.location = {
      lat: data.geometry.location.lat(),
      lng: data.geometry.location.lng()
    };
  }

  href() {
    return `${process.env.REACT_APP_END_POINT}/#!/soon/?location=${this.location}&name=${this.name}`
  }
}