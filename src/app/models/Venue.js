import Place from './Place';

export default class Venue extends Place {
  constructor(data) {
    super(data);

    this.collection = 'venues';
    this.latitude = data.info.geolocation.lat;
    this.longitude = data.info.geolocation.lng;
  }
}