import prodPic from "./homepage-devices/product-image@3x.png";
// features
import compPic from "./features/competitive-illustration.svg";
import storePic from "./features/store-illustration-copy.svg";
import industryPic from "./features/industry-illustration.svg";
// sponsors
import oath from "./sponsors/oath-inc-tagline-blue-transparent-svg@3x.png";
import redbox from "./sponsors/redbox-logo-2017@3x.png";
import caesar from "./sponsors/caesars@3x.png";
import boardwalk from "./sponsors/boardwalk-logo-white-outline-ds@3x.png";
import newmark from "./sponsors/new-mark-horiz-logo-copy@3x.png";
// KeyInsights
import visits from "./keyInsights/visits-icon.svg";
import flowIcon from "./keyInsights/flow-icon.svg";
import audienceIcon from "./keyInsights/audience-icon.svg";
import tradeIcon from "./keyInsights/trade-area-icon.svg";
import churnIcon from "./keyInsights/churn-icon.svg";
import eventsIcon from "./keyInsights/events-icon.svg";
// devices section
import visitorsIcon from "./devices-section/visitors-icon@3x.png";
import devicesIcon from "./devices-section/devices-icon@3x.png";
import venueIcon from "./devices-section/venue-icon@3x.png";
import mobileIcon from "./devices-section/mobile-icon@3x.png";
// api/sdk section
import apiIcon from "./apisdk/api-icon.svg";
import sdkIcon from "./apisdk/sdk-icon.svg";
// banners
import shoppingCenterBanner from "./banners/shopping@2x-min.jpg";
import shoppingCenterMobile from "./banners/shopping-mobile@2x-min.jpg";
// shopping page
import reportPic from "./shopping-page/report-illustration.svg";
import checkmark from "./common/checkmark-icon.svg";
import tabImage from "./shopping-page/foot-traffic-graph.svg";
import plus from "./shopping-page/plus-icon.svg";
import minus from "./shopping-page/minus-icon.svg";
// pricing page
import apiIconVariant from "./pricing-page/api-icon.svg";
import reportIcon from "./pricing-page/report-icon.svg";
import sdkIconVariant from "./pricing-page/sdk-icon.svg";

export {
  prodPic,
  compPic,
  storePic,
  industryPic,
  oath,
  redbox,
  boardwalk,
  caesar,
  newmark,
  visits,
  flowIcon,
  audienceIcon,
  tradeIcon,
  churnIcon,
  eventsIcon,
  visitorsIcon,
  devicesIcon,
  venueIcon,
  mobileIcon,
  apiIcon,
  sdkIcon,
  shoppingCenterBanner,
  shoppingCenterMobile,
  reportPic,
  checkmark,
  tabImage,
  plus,
  minus,
  apiIconVariant,
  reportIcon,
  sdkIconVariant
};
